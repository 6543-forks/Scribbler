import { parseHTML } from 'linkedom';

export const load = async ({ params, fetch }) => {
	const API = `https://dev.to/${params.author}/${params.slug}`;

	const response = await fetch(API);

	if (!response.ok) {
		throw new Error(response.statusText);
	}

	const data = await response.text();
	return parseDevTo(data);
};

const parseDevTo = (html: string) => {
	const { document } = parseHTML(html);

	const header = document.getElementById('main-title');

	const img = header?.querySelector('img')?.src;

	const title = header?.querySelector('h1')?.textContent;

	const article = document.getElementById('article-body')?.innerHTML;

	return {
		img,
		title,
		article
	};
};
