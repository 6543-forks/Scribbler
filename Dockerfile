# Use Alpine as base image
FROM oven/bun:alpine AS build

# Copy project into container
COPY . /app

# Set working directory
WORKDIR /app

# install node modules and build app
RUN --mount=type=cache,target=/app/node_modules \
    bun install --frozen-lockfile && \
    bun run build

#############################################################################

# Use Alpine as base image
FROM oven/bun:alpine

# Copy project into container
COPY --from=build /app/build /app

# Set working directory
WORKDIR /app

# Set needed env vars (can be removed as soon as https://github.com/gornostay25/svelte-adapter-bun/pull/40 is merged)
ENV PROTOCOL_HEADER=x-forwarded-proto
ENV HOST_HEADER=x-forwarded-host

# Execute "bun index.js" command as entrypoint
ENTRYPOINT ["bun", "index.js"]
